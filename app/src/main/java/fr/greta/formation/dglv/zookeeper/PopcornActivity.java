package fr.greta.formation.dglv.zookeeper;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;

import static android.os.Environment.MEDIA_MOUNTED;
import static android.os.Environment.getExternalStoragePublicDirectory;

public class PopcornActivity extends TestActivity {

    private long start2;
    public static final String RESULT_KEY = "t2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        start2 = System.currentTimeMillis();
        super.onCreate(savedInstanceState);
        long t = getIntent().getLongExtra("t", 0);

        String[] tab = getResources().getStringArray(R.array.warn_msg);
        String f = tab[(int)(Math.random()*tab.length)];
        String txt = getString(R.string.pop_msg, t/1000.0, f);
        Toast.makeText(this, txt, Toast.LENGTH_LONG).show();
        if (t>5000) {
            Toast.makeText(this, "DO NOT FEED HIM", Toast.LENGTH_SHORT).show();
        }

        tryLog();

        setContentView(R.layout.popcorn);

    }

    private void tryLog(){
        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            log();
        } else {
            if (!shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            log();
        }

    }

    private void log(){
        Log.i("Popcorn","loooooooooooooooooooooooog");
        if(Environment.getExternalStorageState().equals(MEDIA_MOUNTED)){
            File dir = getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            try {
                FileWriter fw = new FileWriter(new File(dir, "pop.log"), true);
                fw.write("Pop !\n");
                fw.close();
            } catch(Exception ex) {}
        }
    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra(RESULT_KEY, System.currentTimeMillis() - start2);
        setResult(0,i);
        super.onBackPressed();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, MapActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
        return true;

    }
}
