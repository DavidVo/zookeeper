package fr.greta.formation.dglv.zookeeper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;


public class MapActivity extends TestActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("MapActivity", "onCreate()");
        Toast.makeText(this, "Where is the lamb sauce ?", Toast.LENGTH_LONG).show();
        setContentView(new MapView(this));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Intent i = new Intent(this, AquariumActivity.class);
            startActivity(i);
        }
        return true;

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent s = new Intent(Intent.ACTION_VIEW);
        s.setData(Uri.parse("http://www.assemblee-nationale.fr"));
        startActivity(s);
    }

    private class MapView extends View {
        public MapView(Context context) {
            super(context);
        }
        public void onDraw (Canvas c) {
            Bitmap b = ((BitmapDrawable) getResources().getDrawable(R.drawable.map, null)).getBitmap();
            c.drawBitmap(b,0,0,null);
        }
    }

}
