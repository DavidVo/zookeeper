package fr.greta.formation.dglv.zookeeper;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;

public class AnnuaireActivity extends TestActivity {

    private TableLayout tl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.annuaire);

        tl = findViewById(R.id.annuaire_table);

        for(int poo = 0; poo< tl.getChildCount(); poo++){
            TableRow tr = (TableRow)tl.getChildAt(poo);
            for(int poa = 0; poa<tr.getChildCount(); poa++){
                tr.getChildAt(poa).setBackgroundColor(Color.rgb(((poo%2)+(poa%2))*60,205,((poo%2)+(poa%2))*60));
            }
        }



    }
}
