package fr.greta.formation.dglv.zookeeper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.sql.PreparedStatement;

public class Zoohelper extends SQLiteOpenHelper {

    public Zoohelper(Context ctx){
        super(ctx, "Zoo.db", null, 1);
    }

    public void insertAlert(String title, String location, String description, Boolean urgent){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO urgence_zoo (title,location,desicription,urgent) VALUES (?,?,?,?)",
                new Object[]{title,location,description,urgent});
        db.close();
    }

    public String retreiveAlert(String loc){

        SQLiteDatabase db = getReadableDatabase();
        String res = "-";

        Cursor c = db.rawQuery ("SELECT title FROM urgence_zoo WHERE location = ?", new String[]{loc});
        while(c.moveToNext()){
            res += c.getString(0) + "\n";
        }
        db.close();
        return res;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE urgence_zoo(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " title TEXT, location TEXT, desicription TEXT,urgent BOOLEAN);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}


}
