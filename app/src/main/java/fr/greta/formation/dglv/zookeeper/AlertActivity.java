package fr.greta.formation.dglv.zookeeper;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class AlertActivity extends TestActivity {

    private EditText etTitle;
    private EditText etDesciption;
    private CheckBox cb;
    private Button btn;
    private AutoCompleteTextView actvLieu;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.alert);
        etTitle=findViewById(R.id.et_Alert_Title);
        etDesciption=findViewById(R.id.et_Alert_Description);
        cb=findViewById(R.id.cb_Alert);
        btn=findViewById(R.id.btn_Alert_Envoyer);
        actvLieu = findViewById(R.id.actv_Alert_Lieu);

        String[] locations = getResources().getStringArray(R.array.actv_List);
        ArrayAdapter<String> aa = new ArrayAdapter<String>(
                this,android.R.layout.simple_dropdown_item_1line, locations);
        actvLieu.setAdapter(aa);
    }


    public void send(View v){
        String txt = "";
        /**
        if (cb.isChecked()) {
            txt = getString(R.string.msg_envoyer_attention, etTitle.getText(), getString(R.string.tv_alert_envoyer));

            this.finish();
        } else {
            txt = getString(R.string.msg_envoyer, etTitle.getText(), getString(R.string.tv_alert_envoyer));
        }
        **/


        Zoohelper zh = new Zoohelper(this);

        zh.insertAlert(etTitle.getText().toString(), actvLieu.getText().toString(),etDesciption.getText().toString(),
                 cb.isChecked());

        String rxt = zh.retreiveAlert(actvLieu.getText().toString());
        Toast.makeText(this, rxt, Toast.LENGTH_LONG).show();

        this.finish();
    }

    public void ask(View v){
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle(R.string.activity_alert);
        ab.setMessage(R.string.alert_confirm);

        ab.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        ab.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                send(null);
            }
        });

        AlertDialog d=ab.create();
        d.show();
    }
}
