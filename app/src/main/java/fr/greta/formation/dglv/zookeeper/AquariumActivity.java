package fr.greta.formation.dglv.zookeeper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;



public class AquariumActivity extends TestActivity {

    private long startTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new AquariumActivity.AquariumView(this));
        startTime = System.currentTimeMillis();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            Long duration = System.currentTimeMillis() - startTime;
            Intent i = new Intent(this, PopcornActivity.class);
            i.putExtra("t", duration);
            startActivityForResult(i,0);
        }
        return true;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        long t2 = data.getLongExtra(PopcornActivity.RESULT_KEY, 0);
        Log.i("temps : ", t2 + "ms");
        startTime += t2;
    }

    private class AquariumView extends View {
        public AquariumView(Context context) {
            super(context);
        }
        public void onDraw (Canvas c) {
            Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.elys);
            c.drawBitmap(b,0,0,null);
        }
    }

}
