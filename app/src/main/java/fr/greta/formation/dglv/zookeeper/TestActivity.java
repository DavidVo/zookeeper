package fr.greta.formation.dglv.zookeeper;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class TestActivity extends Activity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        MenuItem mi = menu.findItem(R.id.menu_option_enregistrer);
        SharedPreferences sp = getSharedPreferences("zoo", MODE_PRIVATE);
        mi.setChecked(sp.getBoolean("save", false));
        return true;

    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch(item.getItemId()){
            case R.id.menu_carte:
                Intent i = new Intent(this, MapActivity.class);
                startActivity(i);
                return true;
            case R.id.menu_option_enregistrer:
                item.setChecked(!item.isChecked());
                SharedPreferences sp = getSharedPreferences("zoo", MODE_PRIVATE);
                SharedPreferences.Editor e = sp.edit();
                e.putBoolean("save", item.isChecked());
                e.apply();
                return true;

            case R.id.menu_Baladur:
                Log.i("Home", "Recherche:Balladur");
                Thread t = new Thread(){
                  public void run(){
                      try{
                          URL url = new URL("https://fr.wikipedia.org/api/rest_v1/page/summary/Édouard_Balladur");
                          InputStream is =url.openConnection().getInputStream();
                          BufferedReader br = new BufferedReader(new InputStreamReader(is));
                          String t =br.readLine();

                          JSONObject jo = new JSONObject(t);
                          String extract = jo.getString("extract");
                          Toast.makeText(TestActivity.this, extract, Toast.LENGTH_LONG);
                          Log.i("Home", extract);
                          is.close();
                      } catch (Exception ex) {Log.i("Home", "!", ex);}
                  }
                };
                t.start();
                return true;
        }
        return false;
    }
}
