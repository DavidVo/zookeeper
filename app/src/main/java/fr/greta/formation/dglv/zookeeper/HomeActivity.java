package fr.greta.formation.dglv.zookeeper;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HomeActivity extends TestActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        TextView tvNews = findViewById(R.id.tv_news);
        //tvNews.setText("1 verre ça va, 3 verres attention les dégâts"); un moyen d'afficher quelquchose.
        try {
            InputStream is = getAssets().open("news.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String l, t = "";
            while ((l=br.readLine()) != null)
                t += l+"\n";
            is.close();
            tvNews.setText(t);


        } catch(IOException ex) {
            Log.e("Home", "Erreur", ex);

        }

        Button btnMap = findViewById(R.id.btn_map);
        btnMap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, MapActivity.class);
                startActivity(i);
            }
        });

        Button btnAlert = findViewById(R.id.btn_alert);
        btnAlert.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, AlertActivity.class);
                startActivity(i);
            }
        });


        Button btnAnnu = findViewById(R.id.btn_annuaire);
        btnAnnu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, AnnuaireActivity.class);
                startActivity(i);
            }
        });



    }
}
